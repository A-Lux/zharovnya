window.$ = window.jQuery = require("jquery");
require('bootstrap');
require('slick-slider');


let size = 47;
let text = $('.about-dish p');
text.each(function () {
    if ($(this).text().length > size) {
        $(this).text($(this).text().slice(0, size) + '...')
    }
});

$('.quont-minus').click(function () {
    var $input = $(this).parent().find('input');
    var val = +$input[0].defaultValue;
    var count = parseInt($input.val()) - val;
    count = count < val ? val : count;
    $input.val(count);
    $input.change();
    return false;
});
$('.quont-plus').click(function () {
    var $input = $(this).parent().find('input');
    var val = +$input[0].defaultValue;
    let price_all = document.querySelector('.price-all');
    var priceInfo = +price_all.textContent;
    var value = $input.val(parseInt($input.val()) + val);
    // $('.price-all').text((i, val) => +val * Number(value[0].value));
    $input.change();
    return false;
});


ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
        center: [43.26141848, 76.94492184],
        zoom: 11
    }, {
        searchControlProvider: 'yandex#search'
    })
    var myPlacemark = new ymaps.Placemark(
        [43.26141848, 76.94492184]
    );
    myMap.geoObjects.add(myPlacemark);
}
$(document).ready(function(){
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".accordion-icon").addClass("minus").removeClass("plus");
    });
    
    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".card-header").find(".accordion-icon").removeClass("plus").addClass("minus");
    }).on('hide.bs.collapse', function(){
        $(this).prev(".card-header").find(".accordion-icon").removeClass("minus").addClass("plus");
    });
});