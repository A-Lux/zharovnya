<?php include 'main.php' ?>
<?php include 'header.php' ?>
<div class="main-banner">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item"><a href="#">Блюда</a></li>
                <li class="breadcrumb-item active" aria-current="page">Шашлык из баранины</li>
            </ol>
        </nav>
    </div>
</div>
<?php include 'footer.php' ?>