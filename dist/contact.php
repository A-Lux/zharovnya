<?php include 'main.php' ?>
<?php include 'header.php' ?>
<div class="content-page feedback-page">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Обратная связь</li>
            </ol>
        </nav>
        <div class="title">
            <h1>Контакты</h1>
        </div>
        <div class="content-inner">
            <div class="row">
                <div class="col-xl-3">
                    <div class="contact-info">
                        <div class="contact-icon">
                            <img src="images/map-contact.png" alt="">
                        </div>
                        <div class="contact-text">
                            <span>Мы находимся по адресу</span>
                            <p>г. Караганда,
                                ул. Сабыра Рахимова, 138/2
                                (бывш. Луговая)</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="contact-info">
                        <div class="contact-icon">
                            <img src="images/phone-contact.png" alt="">
                        </div>
                        <div class="contact-text">
                            <span>Наш телефон</span>
                            <a href="">8-775-0000-444
                                38-09-09</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="contact-info">
                        <div class="contact-icon">
                            <img src="images/whatsapp2.png" alt="">
                        </div>
                        <div class="contact-text">
                            <span>Наш вотсап</span>
                            <a href="">8-900-231-00-10</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="contact-info">
                        <div class="contact-icon">
                            <img src="images/instagram-2.png" alt="">
                        </div>
                        <div class="contact-text">
                            <span>Мы в инстаграм</span>
                            <a href="">/zharovnya_karaganda</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
    <script src="js/main.js"></script>
</body>

</html>