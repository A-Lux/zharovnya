<?php include 'main.php' ?>
<?php include 'header.php' ?>
<div class="content-page favorite-page">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Избранное</li>
            </ol>
        </nav>
        <div class="title">
            <h1>Избранное</h1>
        </div>
        <div class="content-inner">
        <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card-content">
                            <div class="card-image">
                                <img src="images/card.png" alt="">
                            </div>
                            <div class="card-description">
                                <div class="card-description-title">
                                    <h1>Салат Гавайский
                                        в 2 ряда</h1>
                                    <div class="card-info">
                                        <img src="images/info.png" alt="">
                                    </div>
                                </div>
                                <div class="about-dish">
                                    <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                </div>
                                <div class="about-count">
                                    <div class="count-dish-col">
                                        <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                        <input type="number" name="" id="" value="1">
                                        <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                    </div>
                                    <div class="count-dish">
                                        <p><span class="price-all">1000</span> ₸/<span class='dish-weight'>900</span>г. </p>
                                    </div>
                                </div>
                                <div class="card-bottom">
                                    <div class="card-basket">
                                        <button><img src="images/basket.png" alt="">В корзину</button>
                                    </div>
                                    <div class="favorite-card">
                                        <img src="images/favcard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card-content">
                            <div class="card-image">
                                <img src="images/card.png" alt="">
                            </div>
                            <div class="card-description">
                                <div class="card-description-title">
                                    <h1>Салат Гавайский
                                        в 2 ряда</h1>
                                    <div class="card-info">
                                        <img src="images/info.png" alt="">
                                    </div>
                                </div>
                                <div class="about-dish">
                                    <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                </div>
                                <div class="about-count">
                                    <div class="count-dish-col">
                                        <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                        <input type="number" name="" id="" value="1">
                                        <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                    </div>
                                    <div class="count-dish">
                                        <p><span class="price-all">1000</span> ₸/<span class='dish-weight'>900</span>г. </p>
                                    </div>
                                </div>
                                <div class="card-bottom">
                                    <div class="card-basket">
                                        <button><img src="images/basket.png" alt="">В корзину</button>
                                    </div>
                                    <div class="favorite-card">
                                        <img src="images/favcard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card-content">
                            <div class="card-image">
                                <img src="images/card.png" alt="">
                            </div>
                            <div class="card-description">
                                <div class="card-description-title">
                                    <h1>Салат Гавайский
                                        в 2 ряда</h1>
                                    <div class="card-info">
                                        <img src="images/info.png" alt="">
                                    </div>
                                </div>
                                <div class="about-dish">
                                    <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                </div>
                                <div class="about-count">
                                    <div class="count-dish-col">
                                        <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                        <input type="number" name="" id="" value="1">
                                        <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                    </div>
                                    <div class="count-dish">
                                        <p><span class="price-all">1000</span> ₸/<span class='dish-weight'>900</span>г. </p>
                                    </div>
                                </div>
                                <div class="card-bottom">
                                    <div class="card-basket">
                                        <button><img src="images/basket.png" alt="">В корзину</button>
                                    </div>
                                    <div class="favorite-card">
                                        <img src="images/favcard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card-content">
                            <div class="card-image">
                                <img src="images/card.png" alt="">
                            </div>
                            <div class="card-description">
                                <div class="card-description-title">
                                    <h1>Салат Гавайский
                                        в 2 ряда</h1>
                                    <div class="card-info">
                                        <img src="images/info.png" alt="">
                                    </div>
                                </div>
                                <div class="about-dish">
                                    <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                </div>
                                <div class="about-count">
                                    <div class="count-dish-col">
                                        <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                        <input type="number" name="" id="" value="1">
                                        <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                    </div>
                                    <div class="count-dish">
                                        <p><span class="price-all">1000</span> ₸/<span class='dish-weight'>900</span>г.</p>
                                    </div>
                                </div>
                                <div class="card-bottom">
                                    <div class="card-basket">
                                        <button><img src="images/basket.png" alt="">В корзину</button>
                                    </div>
                                    <div class="favorite-card">
                                        <img src="images/favcard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card-content">
                            <div class="card-image">
                                <img src="images/card.png" alt="">
                            </div>
                            <div class="card-description">
                                <div class="card-description-title">
                                    <h1>Салат Гавайский
                                        в 2 ряда</h1>
                                    <div class="card-info">
                                        <img src="images/info.png" alt="">
                                    </div>
                                </div>
                                <div class="about-dish">
                                    <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                </div>
                                <div class="about-count">
                                    <div class="count-dish-col">
                                        <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                        <input type="number" name="" id="" value="1">
                                        <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                    </div>
                                    <div class="count-dish">
                                        <p><span class="price-all">1000</span> ₸/<span class='dish-weight'>900</span>г. </p>
                                    </div>
                                </div>
                                <div class="card-bottom">
                                    <div class="card-basket">
                                        <button><img src="images/basket.png" alt="">В корзину</button>
                                    </div>
                                    <div class="favorite-card">
                                        <img src="images/favcard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card-content">
                            <div class="card-image">
                                <img src="images/card.png" alt="">
                            </div>
                            <div class="card-description">
                                <div class="card-description-title">
                                    <h1>Салат Гавайский
                                        в 2 ряда</h1>
                                    <div class="card-info">
                                        <img src="images/info.png" alt="">
                                    </div>
                                </div>
                                <div class="about-dish">
                                    <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                </div>
                                <div class="about-count">
                                    <div class="count-dish-col">
                                        <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                        <input type="number" name="" id="" value="1">
                                        <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                    </div>
                                    <div class="count-dish">
                                        <p><span class="price-all">1000</span> ₸/<span class='dish-weight'>900</span>г. </p>
                                    </div>
                                </div>
                                <div class="card-bottom">
                                    <div class="card-basket">
                                        <button><img src="images/basket.png" alt="">В корзину</button>
                                    </div>
                                    <div class="favorite-card">
                                        <img src="images/favcard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>