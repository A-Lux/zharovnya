<?php include 'main.php' ?>
<?php include 'header.php' ?>
<div class="content-page feedback-page">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Обратная связь</li>
            </ol>
        </nav>
        <div class="title">
            <h1>Обратная связь</h1>
            <h2>Руководство</h2>
        </div>
        <div class="content-inner">
            <p> <b>Номер руководства</b></p>
            <div class="whatsapp-num">
                <p><img src="images/whatsapp1.png" alt=""> +7 (708) 100-00-02</p>
                <span>Ваши жалобы, отзывы и пожелания касательно качества продукции и обслуживания очень важны для нас</span>
            </div>
            <div class="title">
                <h2>Вопросы и ответы</h2>
            </div>
            <div class="accordion" id="accordion-card">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                Есть ли у вас шеф-повар?
                                <div class="plus accordion-icon"></div>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordion-card">
                        <div class="card-body">
                            <p>В кафе-ресторане «Жаровня» действуют персональные карты со скидками 10 и 15%. Скидка по
                                карте действует на основное меню. При предъявлении удостоверения ОАО «КАМАЗ»
                                предоставляется скидка 10%. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <div class="plus accordion-icon"></div>
                                Как вы разбираетесь с вопросами безопасности?
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion-card">
                        <div class="card-body">
                            <p>В кафе-ресторане «Жаровня» действуют персональные карты со скидками 10 и 15%. Скидка по
                                карте действует на основное меню. При предъявлении удостоверения ОАО «КАМАЗ»
                                предоставляется скидка 10%. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <div class="plus accordion-icon"></div>
                                Какие скидки есть?
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                        data-parent="#accordion-card">
                        <div class="card-body">
                            <p>В кафе-ресторане «Жаровня» действуют персональные карты со скидками 10 и 15%. Скидка по
                                карте действует на основное меню. При предъявлении удостоверения ОАО «КАМАЗ»
                                предоставляется скидка 10%. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title">
                <h2>Задать вопрос</h2>
            </div>
            <div class="questions-inner">
                <div class="questions-form">
                    <p>Ваше имя</p>
                    <input type="text">
                </div>
                <div class="questions-form">
                    <p>Ваш телефон или e-mail</p>
                    <input type="text">
                </div>
                <div class="questions-form">
                    <p>Ваш вопрос</p>
                    <textarea name="" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="questions-form">
                    <button>Отправить</button>
                </div>
                <div class="title">
                    <h2>Хотите с нами
                        сотрудничать?</h2>
                </div>
                <div class="request-btn">
                    <button>Оставить заявку</button>
                </div>
            </div>

        </div>
    </div>
</div>
<?php include 'footer.php' ?>