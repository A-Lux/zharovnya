<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-12 col-md-6">
                <div id="map">

                </div>
            </div>
            <div class="col-xl-6 col-12 col-md-6">
                <div class="footer-inner">
                    <h1>Контакты</h1>
                    <div class="contact-inner">
                        <div class="contact-icon">
                            <img src="images/phone.png" alt="">
                        </div>
                        <div class="contact-info">
                            <p>Контакты:</p>
                            <a href="">+7 (7212) 38-09-09</a>
                        </div>
                    </div>
                    <div class="contact-inner">
                        <div class="contact-icon">
                            <img src="images/whatsaap.png" alt="">
                        </div>
                        <div class="contact-info">
                            <p>Whatsapp:</p>
                            <a href="">+7-775-000-04-44</a>
                        </div>
                    </div>
                    <div class="contact-inner">
                        <div class="contact-icon">
                            <img src="images/mail.png" alt="">
                        </div>
                        <div class="contact-info">
                            <p>Пишите нам:</p>
                            <a href="">restaurant_zharovnya@mail.ru</a>
                        </div>
                    </div>
                    <div class="contact-inner">
                        <div class="contact-icon">
                            <img src="images/map.png" alt="">
                        </div>
                        <div class="contact-info">
                            <p>Мы находимся:</p>
                            <span>г. Караганда, ул. Сабыра Рахимова
                                138/2
                                (бывш.Луговая)</span>
                        </div>
                    </div>
                    <div class="feedback">
                        <a href="">Обратная связь</a>
                    </div>
                    <div class="copyright">
                        <p>©  Жаровня, 2020.  Все права защищены.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
    <script src="js/main.js"></script>
</body>

</html>