<div class="header-inner">
    <div class="container">
    <div class="row">
        <div class="col-xl-2 col-4 col-md-3">
            <div class="header-left-inner">
                <div class="burger-menu">
                    <a href=""><img src="images/burger.png" alt=""></a>
                </div>
                <div class="account">
                    <a href=""><img src="images/account.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-3 col-md-6">
            <div class="logo">
                <a href="">
                    <img src="images/header-logo.png" alt="">
                </a>
            </div>
        </div>
        <div class="col-xl-2 col-5 col-md-3">
            <div class="header-inner-right">
                <div class="search">
                    <img src="images/search.svg" alt="">
                </div>
                <div class="favorite">
                    <a href=""><img src="images/favorite.png" alt=""></a>
                </div>
                <div class="basket">
                    <a href=""><img src="images/basket.png" alt=""></a>
                    <span>1</span>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>