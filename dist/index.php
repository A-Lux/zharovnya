<?php include 'main.php' ?>
<div class="banner">
        <div class="container">
            <div class="header">
                <div class="row">
                    <div class="col-xl-2 col-4 col-md-3">
                        <div class="header-left-inner">
                            <div class="burger-menu">
                                <a href=""><img src="images/burger.png" alt=""></a>
                            </div>
                            <div class="account">
                                <a href=""><img src="images/account.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-3 col-md-6">
                        <div class="logo">
                            <a href="">
                                <img src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-2 col-5 col-md-3">
                        <div class="header-inner-right">
                            <div class="search">
                                <img src="images/search.svg" alt="">
                            </div>
                            <div class="favorite">
                                <a href=""><img src="images/favorite.png" alt=""></a>
                            </div>
                            <div class="basket">
                                <a href=""><img src="images/basket.png" alt=""></a>
                                <span>1</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-content">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="banner-inner">
                            <h1>Вкусно и душевно провести
                                время в кафе-ресторане
                                «Жаровня»</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua.</p>
                            <div class="content-btn">
                                <a href="">Перейти к каталогу</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-top">
        <div class="container">
            <div class="main-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="new-tab" data-toggle="tab" href="#new" role="tab"
                            aria-controls="new" aria-selected="true">Новинки</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="surprise-tab" data-toggle="tab" href="#surprise" role="tab"
                            aria-controls="surprise" aria-selected="false">Сюрприз</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="dishes-tab" data-toggle="tab" href="#dishes" role="tab"
                            aria-controls="dishes" aria-selected="false">Блюда</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="confectionery-tab" data-toggle="tab" href="#confectionery" role="tab"
                            aria-controls="confectionery" aria-selected="false">Кондитерские изделия</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="dishes-tab" data-toggle="tab" href="#dishes" role="tab"
                            aria-controls="dishes" aria-selected="false">Блюда</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="bar-tab" data-toggle="tab" href="#bar" role="tab" aria-controls="bar"
                            aria-selected="false">Барное меню</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="banket-tab" data-toggle="tab" href="#banket" role="tab"
                            aria-controls="banket" aria-selected="false">Банкетные блюда</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="container">
            <div class="tabs-inner">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="new" role="tabpanel" aria-labelledby="new-tab">
                        <div class="subtitle">
                            <h1>Новинки</h1>
                        </div>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="images/card.png" alt="">
                                    </div>
                                    <div class="card-description">
                                        <div class="card-description-title">
                                            <h1>Салат Гавайский
                                                в 2 ряда</h1>
                                            <div class="card-info">
                                                <img src="images/info.png" alt="">
                                            </div>
                                        </div>
                                        <div class="about-dish">
                                            <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                        </div>
                                        <div class="about-count">
                                            <div class="count-dish-col">
                                                <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                <input type="number" name="" id="" value="1">
                                                <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                            </div>
                                            <div class="count-dish">
                                                <p><span class="price-all">1000</span> ₸ </p>
                                            </div>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="card-basket">
                                                <button><img src="images/basket.png" alt="">В корзину</button>
                                            </div>
                                            <div class="favorite-card">
                                                <img src="images/favcard.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="look-all">
                            <p>
                                <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button"
                                    aria-expanded="false" aria-controls="collapseExample">
                                    Показать еще
                                </a>
                            </p>
                        </div>
                        <div class="collapse" id="collapseExample">
                            <div class="row">
                                <div class="col-xl-3 col-md-6">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="images/card.png" alt="">
                                        </div>
                                        <div class="card-description">
                                            <div class="card-description-title">
                                                <h1>Салат Гавайский
                                                    в 2 ряда</h1>
                                                <div class="card-info">
                                                    <img src="images/info.png" alt="">
                                                </div>
                                            </div>
                                            <div class="about-dish">
                                                <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                            </div>
                                            <div class="about-count">
                                                <div class="count-dish-col">
                                                    <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                    <input type="number" name="" id="" value="1">
                                                    <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                                </div>
                                                <div class="count-dish">
                                                    <p><span class="price-all">1000</span> ₸ </p>
                                                </div>
                                            </div>
                                            <div class="card-bottom">
                                                <div class="card-basket">
                                                    <button><img src="images/basket.png" alt="">В корзину</button>
                                                </div>
                                                <div class="favorite-card">
                                                    <img src="images/favcard.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="images/card.png" alt="">
                                        </div>
                                        <div class="card-description">
                                            <div class="card-description-title">
                                                <h1>Салат Гавайский
                                                    в 2 ряда</h1>
                                                <div class="card-info">
                                                    <img src="images/info.png" alt="">
                                                </div>
                                            </div>
                                            <div class="about-dish">
                                                <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                            </div>
                                            <div class="about-count">
                                                <div class="count-dish-col">
                                                    <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                    <input type="number" name="" id="" value="1">
                                                    <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                                </div>
                                                <div class="count-dish">
                                                    <p><span class="price-all">1000</span> ₸ </p>
                                                </div>
                                            </div>
                                            <div class="card-bottom">
                                                <div class="card-basket">
                                                    <button><img src="images/basket.png" alt="">В корзину</button>
                                                </div>
                                                <div class="favorite-card">
                                                    <img src="images/favcard.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="images/card.png" alt="">
                                        </div>
                                        <div class="card-description">
                                            <div class="card-description-title">
                                                <h1>Салат Гавайский
                                                    в 2 ряда</h1>
                                                <div class="card-info">
                                                    <img src="images/info.png" alt="">
                                                </div>
                                            </div>
                                            <div class="about-dish">
                                                <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                            </div>
                                            <div class="about-count">
                                                <div class="count-dish-col">
                                                    <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                    <input type="number" name="" id="" value="1">
                                                    <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                                </div>
                                                <div class="count-dish">
                                                    <p><span class="price-all">1000</span> ₸ </p>
                                                </div>
                                            </div>
                                            <div class="card-bottom">
                                                <div class="card-basket">
                                                    <button><img src="images/basket.png" alt="">В корзину</button>
                                                </div>
                                                <div class="favorite-card">
                                                    <img src="images/favcard.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="images/card.png" alt="">
                                        </div>
                                        <div class="card-description">
                                            <div class="card-description-title">
                                                <h1>Салат Гавайский
                                                    в 2 ряда</h1>
                                                <div class="card-info">
                                                    <img src="images/info.png" alt="">
                                                </div>
                                            </div>
                                            <div class="about-dish">
                                                <p>курица копченная, ананас, помидор, сыр, майонез, майонез</p>
                                            </div>
                                            <div class="about-count">
                                                <div class="count-dish-col">
                                                    <span class="quont-minus"><img src="images/minus.png" alt=""></span>
                                                    <input type="number" name="" id="" value="1">
                                                    <span class="quont-plus"><img src="images/plus.png" alt=""></span>
                                                </div>
                                                <div class="count-dish">
                                                    <p><span class="price-all">1000</span> ₸ </p>
                                                </div>
                                            </div>
                                            <div class="card-bottom">
                                                <div class="card-basket">
                                                    <button><img src="images/basket.png" alt="">В корзину</button>
                                                </div>
                                                <div class="favorite-card">
                                                    <img src="images/favcard.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="surprise" role="tabpanel" aria-labelledby="surprise-tab">...</div>
                    <div class="tab-pane fade" id="dishes" role="tabpanel" aria-labelledby="dishes-tab">...</div>
                    <div class="tab-pane fade" id="confectionery" role="tabpanel" aria-labelledby="confectionery-tab">
                        ...</div>
                    <div class="tab-pane fade" id="bar" role="tabpanel" aria-labelledby="bar-tab">...</div>
                    <div class="tab-pane fade" id="banket" role="tabpanel" aria-labelledby="banket-tab">...</div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-company">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="about-text">
                        <h1>Немного о нас</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo
                            viverra maecenas accumsan lacus vel facilisis. </p>
                        <a href="">Наши контакты</a>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="about-company-image">
                        <img src="images/about.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php' ?>