const path = require('path');
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
    mode: "development",
    entry: ['./app/src/js/main.js', './app/src/js/app.js'],
    output: {
        filename: 'main.js',
        path: path.join(__dirname, './dist/js')
    },
    module: {
        rules: [
            {
                test: /\.(scss)$/,
                use: [
                {
                    loader: 'style-loader'
                },
                {
                    loader: 'css-loader'
                },
                {
                    loader: 'postcss-loader',
                    options: {
                    plugins: function () {
                        return [
                        require('autoprefixer')
                        ];
                    }
                    }
                },
                {
                    loader: 'sass-loader'
                }
                ]
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.scss$/,
                use: ["vue-style-loader", "css-loader", "sass-loader"],
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
            }
            
        ],
        
    },
    plugins: [new VueLoaderPlugin()],
    resolve: {
        alias: {
        vue: "vue/dist/vue.js",
        },
    },
};
